import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

/**
 * Created by Dmitry Teplyakov on 28.06.17.
 */
public class GenAlg {
    private static double fPow(float a, float b) {
        if(b == 0)  return 1;
        return a * fPow(a, b-1);
    }
    private static double calcFunc(int a, int b, int c, int d, Individ individForCalc) {
        int x = individForCalc.getFenotype();
        double res =  a+b*x + c*fPow(x, 2) + d*fPow(x, 3);
        //System.out.println(res + " " + x);
        return res;
    }

    public static void main(String args[]) {
        Random random = new Random();
        ArrayList<Individ> populationList = new ArrayList<>();
        int a, b, c, d, iterations;
        a = 28;
        b = -28;
        c = -64;
        d = 1;
        iterations = 50;
        for(int i = 0; i < 4; i++)
            populationList.add(new Individ(random.nextInt()));
        ArrayList<Individ> childrenIndivid = new ArrayList<>();
        System.out.println(String.format("%8s|%8s", Integer.toBinaryString(populationList.get(0).getChromosome()), Integer.toBinaryString(populationList.get(1).getChromosome())).replace(' ', '0'));
        Individ min = null;
        Individ max = null;
        int num = 4;
        int counter = 0;
        int locRand;
        while(counter != iterations) {

            int firstRand, secondRand, count = 0;
            while (count < 2) {
                firstRand = random.nextInt(4);
                secondRand = random.nextInt(4);
                if (populationList.get(firstRand).getId() != populationList.get(secondRand).getId()) {
                    childrenIndivid.add(new Individ(populationList.get(firstRand), populationList.get(secondRand)));
                    childrenIndivid.add(new Individ(populationList.get(secondRand), populationList.get(firstRand)));
                    count++;
                }
                if(count == 2) { locRand = random.nextInt(num); childrenIndivid.get(locRand).mutation(); System.out.println("MUTATION in element: " + locRand + " After mutation: " + String.format("%8s", Integer.toBinaryString(childrenIndivid.get(locRand).getChromosome())).replace(' ', '0')); }
            }

                for (Individ ind : populationList)
                    ind.setResult(calcFunc(a, b, c, d, ind));

                for (Individ ind : childrenIndivid)
                    ind.setResult(calcFunc(a, b, c, d, ind));

                if(counter < iterations/2) {
                    int ind;
                    for (int i = 1; i < populationList.size(); i++) {
                        ind = i - 1;
                        while ((ind >= 0) && (populationList.get(ind).getResult() > populationList.get(ind + 1).getResult())) {
                            Collections.swap(populationList, ind, ind + 1);
                            ind--;
                        }
                    }
                    for (int i = 1; i < childrenIndivid.size(); i++) {

                        ind = i - 1;
                        while ((ind >= 0) && (childrenIndivid.get(ind).getResult() > childrenIndivid.get(ind + 1).getResult())) {
                            Collections.swap(childrenIndivid, ind, ind + 1);
                            ind--;
                        }
                    }
                        max = populationList.get(3);
                }
                else {
                    int ind;
                    for (int i = 1; i < populationList.size(); i++) {

                        ind = i - 1;
                        while ((ind >= 0) && (populationList.get(ind).getResult() < populationList.get(ind + 1).getResult())) {
                            Collections.swap(populationList, ind, ind + 1);
                            ind--;
                        }
                    }
                    for (int i = 1; i < childrenIndivid.size(); i++) {
                        ind = i - 1;
                        while ((ind >= 0) && (childrenIndivid.get(ind).getResult() < childrenIndivid.get(ind + 1).getResult())) {
                            Collections.swap(childrenIndivid, ind, ind + 1);
                            ind--;
                        }
                    }
                    min = populationList.get(3);
                }

                for (int i = 0; i < 2; i++) {
                    populationList.remove(0);
                    childrenIndivid.remove(0);
                }
                for (Individ indiv : populationList)
                    System.out.println("PopulationList Sorted: " + indiv.getResult() + " Fenotype: " + indiv.getFenotype());
                for (Individ indiv : childrenIndivid)
                    System.out.println("ChildrenIndivid Sorted: " + indiv.getResult() + " Fenotype: " + indiv.getFenotype());

                populationList.add(childrenIndivid.get(0));
                populationList.add(childrenIndivid.get(1));

                for (int i = 0; i < 2; i++) {
                    childrenIndivid.remove(0);
                }
            counter++;
        }
        System.out.println("MIN Func: " + min.getResult() + " Chromosome: " + String.format("%6s", Integer.toBinaryString(min.getChromosome())).replace(' ', '0') + " Fenotype: " + min.getFenotype());
        System.out.println("MAX Func: " + max.getResult() + " Chromosome: " + String.format("%6s", Integer.toBinaryString(max.getChromosome())).replace(' ', '0') + " Fenotype: " + max.getFenotype());
    }
}
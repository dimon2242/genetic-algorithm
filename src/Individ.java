import java.util.Random;
import java.util.UUID;

/**
 * Created by Dmitry Teplyakov on 28.06.17.
 */
public class Individ {
    private UUID id;
    private double result;
    private byte chromosome;
    private boolean isMutated;


    Individ(int decChromosome) {
        id = UUID.randomUUID();
        chromosome = (byte) (decChromosome & ~192);
        isMutated = false;
        result = 0;
    }

    public boolean isMutated() {
        return isMutated;
    }

    Individ(Individ firstParent, Individ secondParent) {
        id = UUID.randomUUID();
        chromosome = (byte) ((pairChromosome(firstParent.getChromosome(), secondParent.getChromosome())) & ~192);
        isMutated = false;
        result = 0;
    }
    byte getChromosome() {
        return chromosome;
    }
    UUID getId() {
        return id;
    }
    void setResult(double result) {
        this.result = result;
    }
    double getResult() {
        return result;
    }
    byte pairChromosome(byte chfParent, byte chsParent) {
        byte chromosome = chfParent;
        chromosome &= 240;
        chromosome |= ((chsParent & 15));
        System.out.println(String.format("%6s", Integer.toBinaryString(chromosome)).replace(' ', '0'));
        return (byte) (chromosome & ~192);
    }
    int getFenotype() {
        return chromosome - 10;
    }
    void mutation() {
        Random random = new Random();
        int offset = random.nextInt(6);
        int mask = (1 << offset);
        chromosome ^= mask; // Invert random bit
        //System.out.println(offset); // debug ;)
        isMutated = true;
    }

}
